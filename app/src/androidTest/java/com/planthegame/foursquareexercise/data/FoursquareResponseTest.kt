package com.planthegame.foursquareexercise.data

import android.support.test.runner.AndroidJUnit4
import org.json.JSONArray
import org.json.JSONObject
import org.junit.Assert
import org.junit.Test

import org.junit.runner.RunWith

/**
 * Unit tests for [FoursquareResponse]
 */
@RunWith(AndroidJUnit4::class)
class FoursquareResponseTest {
    @Test
    fun getResponseCode_available() {
        val json = JSONObject().put("meta", JSONObject().put("code", 203))

        Assert.assertEquals(203, FoursquareResponse(json).responseCode)
    }

    @Test
    fun getResponseCode_unavailable() {
        val json = JSONObject().put("code", 203)

        Assert.assertEquals(0, FoursquareResponse(json).responseCode)
    }

    @Test
    fun getVenues_threeVenues() {

        val venue1 = JSONObject().put("name", "abc1")
        val venue2 = JSONObject().put("name", "abc2")
        val venue3 = JSONObject().put("name", "abc3")
        val venueArray = JSONArray().put(venue1).put(venue2).put(venue3)
        val venuesObject = JSONObject().put("venues", venueArray)
        val json = JSONObject().put("response", venuesObject).put("meta", JSONObject().put("code", 200))

        Assert.assertEquals(3, FoursquareResponse(json).venues.size)
    }

    @Test
    fun getVenues_venueData() {

        val location = JSONObject().put("address", "street 1").put("distance", 1111)
        val venue = JSONObject().put("name", "venue name").put("location", location)
        val venueArray = JSONArray().put(venue)
        val venuesObject = JSONObject().put("venues", venueArray)
        val json = JSONObject().put("response", venuesObject).put("meta", JSONObject().put("code", 200))

        with(FoursquareResponse(json)) {
            Assert.assertEquals("venue name", venues[0].name)
            Assert.assertEquals("street 1", venues[0].address)
            Assert.assertEquals(1111, venues[0].distance)
        }
    }

    @Test
    fun getVenues_venueWithoutLocation() {

        val venue = JSONObject().put("name", "abc")
        val venueArray = JSONArray().put(venue)
        val venuesObject = JSONObject().put("venues", venueArray)
        val json = JSONObject().put("response", venuesObject).put("meta", JSONObject().put("code", 200))

        with (FoursquareResponse(json)) {
            Assert.assertNotNull(venues[0].name)
            Assert.assertNull(venues[0].address)
            Assert.assertNull(venues[0].distance)
        }
    }

    @Test
    fun getVenues_venueWithoutStreetAddress() {

        val location = JSONObject().put("city", "Citi").put("distance", 1111)
        val venue = JSONObject().put("name", "abc").put("location", location)
        val venueArray = JSONArray().put(venue)
        val venuesObject = JSONObject().put("venues", venueArray)
        val json = JSONObject().put("response", venuesObject).put("meta", JSONObject().put("code", 200))

        Assert.assertEquals("Citi", FoursquareResponse(json).venues[0].address)
    }
}