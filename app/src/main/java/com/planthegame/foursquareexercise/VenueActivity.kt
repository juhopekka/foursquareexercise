package com.planthegame.foursquareexercise

import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.R.layout.simple_list_item_1
import android.content.pm.PackageManager.PERMISSION_GRANTED
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.widget.ArrayAdapter
import com.planthegame.foursquareexercise.data.Venue
import com.planthegame.foursquareexercise.data.source.GpsLocationDataSource
import com.planthegame.foursquareexercise.data.source.FoursquareVenueDataSource
import kotlinx.android.synthetic.main.activity_venue.*

/**
 * The only activity of the app containing the search input field and the list of found venues.
 */
class VenueActivity : AppCompatActivity(), VenueContract.View {

    private lateinit var presenter: VenueContract.Presenter
    private lateinit var adapter: ArrayAdapter<Venue>
    private val venueList = mutableListOf<Venue>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_venue)

        adapter = ArrayAdapter(this, simple_list_item_1, venueList)
        venue_listview.adapter = adapter
        search_word_value_edittext.addTextChangedListener(textWatcher)

        presenter = VenuePresenter(this, FoursquareVenueDataSource(), GpsLocationDataSource(this))
    }

    override fun isLocationPermissionGranted()
            = ContextCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) == PERMISSION_GRANTED

    override fun askForPermission() {
        ActivityCompat.requestPermissions(this, arrayOf(ACCESS_FINE_LOCATION), PERMISSION_REQUEST)
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<out String>,
                                            grantResults: IntArray) {
        if (requestCode == PERMISSION_REQUEST) {
            presenter.onPermissionResult(grantResults.isNotEmpty()
                    && grantResults[0] == PERMISSION_GRANTED)
        }
    }

    override fun shutdownWithLackOfPermissionNote() {
        AlertDialog.Builder(this).setTitle("Wrong answer")
                .setMessage("The app is useless without the permission. Shutting down...")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton("OK") { _, _ -> finish() }
                .show()
    }

    override fun showVenues(venues: Array<Venue>) {
        venueList.clear()
        venueList.addAll(venues)
        runOnUiThread {
            adapter.notifyDataSetChanged()
        }
    }

    private val textWatcher = object : TextWatcher {
        override fun afterTextChanged(text: Editable?) {
            if (text != null) presenter.searchWordSet(text.toString())
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
    }

    companion object {
        private const val PERMISSION_REQUEST: Int = 123
    }
}
