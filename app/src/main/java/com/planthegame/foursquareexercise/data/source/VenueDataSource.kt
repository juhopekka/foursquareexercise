package com.planthegame.foursquareexercise.data.source

import android.location.Location
import com.planthegame.foursquareexercise.util.ResultCallback

/**
 * Data source for venues
 */
interface VenueDataSource {
    /**
     * Get venues from the venue source
     *
     * @param searchWord A search term to be applied against venue names.
     * @param location Location of the user
     * @param resultCallback The callback function to execute when result is available
     */
    fun getVenuesFor(searchWord: String,
                     location: Location,
                     resultCallback: ResultCallback)
}
