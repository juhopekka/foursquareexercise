package com.planthegame.foursquareexercise.data.source

import android.location.Location

/**
 * Data source for location
 */
interface LocationDataSource {
    /**
     * Location data can be activated once the location permissions are ok.
     *
     * @return True if activating succeeded
     */
    fun activate(): Boolean

    /**
     * Contains current location or null if not known
     */
    var currentLocation: Location?
}