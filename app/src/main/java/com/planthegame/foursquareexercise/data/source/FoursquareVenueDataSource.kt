package com.planthegame.foursquareexercise.data.source

import android.location.Location
import android.util.Log
import com.planthegame.foursquareexercise.data.FoursquareResponse
import com.planthegame.foursquareexercise.util.ResultCallback
import com.planthegame.foursquareexercise.util.toRequestParameterFormat
import okhttp3.*
import org.json.JSONObject
import java.io.IOException

/**
 * Data source implementation to get venues from Foursquare service
 */
open class FoursquareVenueDataSource : VenueDataSource {

    private val client = OkHttpClient()

    override fun getVenuesFor(searchWord: String,
                              location: Location,
                              resultCallback: ResultCallback) {

        val url = urlBuilder(searchEndpoint)
                .addEncodedQueryParameter(locationKey, location.toRequestParameterFormat())
                .addQueryParameter(queryKey, searchWord).build()

        val request = Request.Builder().url(url).build()

        client.newCall(request).enqueue(ResponseCallback(resultCallback))
    }

    class ResponseCallback(private val resultCallback: ResultCallback) : Callback {

        override fun onResponse(call: Call?, response: Response?) {
            // Handle only the happy case
            if (response?.code() == 200) {
                response.body()?.string().run {
                    val venues = FoursquareResponse(JSONObject(this)).venues
                    resultCallback.invoke(venues)
                }
            } else {
                Log.e("Venues", "Unexpected response code: ${response?.code()}")
            }
        }

        override fun onFailure(call: Call, e: IOException?) {
            Log.e("Venues", "Failed to make request: ${call.request()?.url()}", e)
        }
    }

    private fun urlBuilder(endpoint: String) = HttpUrl.Builder()
            .scheme(scheme).host(host).addPathSegments(api)
            .addPathSegment(endpoint)
            .addQueryParameter(versionKey, versionValue)
            .addQueryParameter(clientIdKey, clientIdValue)
            .addQueryParameter(clientSecretKey, clientSecretValue)

    companion object {
        private const val scheme            = "https"
        private const val host              = "api.foursquare.com"
        private const val api               = "v2/venues"
        private const val searchEndpoint    = "search"

        private const val versionKey        = "v"
        private const val clientIdKey       = "client_id"
        private const val clientSecretKey   = "client_secret"
        private const val locationKey       = "ll"
        private const val queryKey          = "query"

        private const val versionValue     = "20180503"
        private const val clientIdValue     = "CYEMKOM4OLTP5PHMOFVUJJAMWT5CH5G1JBCYREATW21XLLSZ"
        private const val clientSecretValue = "-----"
    }
}