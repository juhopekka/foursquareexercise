package com.planthegame.foursquareexercise.data

import android.util.Log
import org.json.JSONException
import org.json.JSONObject

/**
 * Class for providing the response data of 'search' request made to Foursquare service
 */
class FoursquareResponse(response: JSONObject) {

    @Suppress("MemberVisibilityCanBePrivate")
    val responseCode = parseResponseCode(response)
    val venues = if (responseCode == 200) parseVenuesFromResponse(response)
                 else emptyArray()

    private fun parseResponseCode(response: JSONObject) = try {
            response.getJSONObject(META_OBJ).getInt(META_CODE_INT)
        } catch (e: JSONException) {
            Log.e(TAG, "Exception while parsing response code", e)
            0
        }

    private fun parseVenuesFromResponse(responseJson: JSONObject): Array<Venue> {
        val venueArray = arrayListOf<Venue>()
        try {
            val venuesJSONArray = responseJson.getJSONObject(RESPONSE_OBJ).getJSONArray(RESPONSE_VENUES_ARRAY)
            (0 until venuesJSONArray.length())
                    .map { venuesJSONArray.getJSONObject(it) }
                    //.sortedBy { it.getJSONObject("location").getInt("distance") }
                    .forEach {
                        venueArray.add(Venue(parseName(it), resolveAddress(it), parseDistance(it)))
                    }
        } catch (e: JSONException) {
            Log.e(TAG, "Exception while parsing venues from response", e)
        }
        return venueArray.toTypedArray()
    }

    private fun parseName(venueJson: JSONObject) = try {
        venueJson.getString(VENUE_NAME_STR)
    } catch (e: JSONException) {
        Log.e(TAG, "Exception while parsing name from venue object", e)
        null
    }

    private fun resolveAddress(venueJson: JSONObject) = if (venueJson.has(VENUE_LOCATION_OBJ)) {
        val location = venueJson.getJSONObject(VENUE_LOCATION_OBJ)
        if (venueJson.has(VENUE_FUZZED_BOOL) && location.getBoolean(VENUE_FUZZED_BOOL)) {
            "The location is fuzzed"
        } else if (location.has(VENUE_LOCATION_ADDRESS_STR)) {
            location.getString(VENUE_LOCATION_ADDRESS_STR)
        } else if (location.has(VENUE_LOCATION_CITY_STR)) {
            location.getString(VENUE_LOCATION_CITY_STR)
        } else if (location.has(VENUE_LOCATION_COUNTRY_STR)) {
            location.getString(VENUE_LOCATION_COUNTRY_STR)
        } else {
            null
        }
    } else {
        null
    }

    private fun parseDistance(venueJson: JSONObject) = try {
        venueJson.getJSONObject(VENUE_LOCATION_OBJ).getInt(VENUE_LOCATION_DISTANCE_INT)
    } catch (e: JSONException) {
        Log.e(TAG, "Exception while parsing distance from venue object", e)
        null
    }

    companion object {
        private const val TAG = "asd"

        private const val META_OBJ                    = "meta"
        private const val META_CODE_INT               = "code"

        private const val RESPONSE_OBJ                = "response"
        private const val RESPONSE_VENUES_ARRAY       = "venues"

        private const val VENUE_NAME_STR              = "name"
        private const val VENUE_FUZZED_BOOL           = "isFuzzed"
        private const val VENUE_LOCATION_OBJ          = "location"
        private const val VENUE_LOCATION_ADDRESS_STR  = "address"
        private const val VENUE_LOCATION_CITY_STR     = "city"
        private const val VENUE_LOCATION_COUNTRY_STR  = "country"
        private const val VENUE_LOCATION_DISTANCE_INT = "distance"
    }
}