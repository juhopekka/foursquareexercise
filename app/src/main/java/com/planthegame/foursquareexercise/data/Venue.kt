package com.planthegame.foursquareexercise.data

/**
 * Data class to present a single venue
 */
@Suppress("MemberVisibilityCanBePrivate")
data class Venue(val name: String?,
                 val address: String?,
                 val distance: Int?) {

    override fun toString(): String {
        return "Name: $name\nAddress: $address\nDistance: $distance m"
    }

}
