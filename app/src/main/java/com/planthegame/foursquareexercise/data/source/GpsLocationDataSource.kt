package com.planthegame.foursquareexercise.data.source

import android.content.Context
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.location.LocationManager.GPS_PROVIDER
import android.location.LocationManager.NETWORK_PROVIDER
import android.os.Bundle
import junit.framework.Assert

/**
 * Data source implementation to get current location from GPS
 */
open class GpsLocationDataSource(context: Context) : LocationDataSource {

    private val locationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager

    override var currentLocation: Location? = null

    override fun activate() = try {
        currentLocation = locationManager.getLastKnownLocation(NETWORK_PROVIDER)
        locationManager.requestLocationUpdates(GPS_PROVIDER,
                LOCATION_MIN_UPDATE_TIME_MS, LOCATION_MIN_UPDATE_DISTANCE_M,
                locationListener)
        true
    } catch (e: SecurityException) {
        Assert.fail("Permissions should be handled before activating this")
        false
    }

    private val locationListener = object : LocationListener {

        override fun onLocationChanged(location: Location?) {
            currentLocation = location
        }

        override fun onStatusChanged(p0: String?, p1: Int, p2: Bundle?) {}
        override fun onProviderEnabled(p0: String?) {}
        override fun onProviderDisabled(p0: String?) {}
    }

    companion object {
        private const val LOCATION_MIN_UPDATE_TIME_MS    = 30 * 1000L
        private const val LOCATION_MIN_UPDATE_DISTANCE_M = 10f
    }
}