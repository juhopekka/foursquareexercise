package com.planthegame.foursquareexercise

import com.planthegame.foursquareexercise.data.Venue

/**
 * Contract of the only view of the app
 */
interface VenueContract {

    interface View {
        fun isLocationPermissionGranted(): Boolean
        fun askForPermission()
        fun shutdownWithLackOfPermissionNote()
        fun showVenues(venues: Array<Venue>)
    }

    interface Presenter {
        fun onPermissionResult(granted: Boolean)
        fun searchWordSet(searchWord: String)
    }
}