package com.planthegame.foursquareexercise

import com.planthegame.foursquareexercise.data.source.GpsLocationDataSource
import com.planthegame.foursquareexercise.data.source.FoursquareVenueDataSource

/**
 * Presenter of [VenueActivity]
 */
class VenuePresenter(private val venueView: VenueContract.View,
                     private val venueDataSource: FoursquareVenueDataSource,
                     private val locationDataSource: GpsLocationDataSource)
    : VenueContract.Presenter {

    private var latestSearchWord: String? = null

    init {
        if (venueView.isLocationPermissionGranted()) locationDataSource.activate()
        else venueView.askForPermission()
    }

    override fun onPermissionResult(granted: Boolean) {
       if (!granted || !locationDataSource.activate()) {
          venueView.shutdownWithLackOfPermissionNote()
       }
    }

    override fun searchWordSet(searchWord: String) {
        latestSearchWord = searchWord
        val location = locationDataSource.currentLocation
        if (searchWord.isEmpty()) {
            venueView.showVenues(emptyArray())
        } else if (location != null) {
            venueDataSource.getVenuesFor(searchWord, location, {
                foundVenues ->
                // Don't show old results if response is slower than the user...
                if (latestSearchWord == searchWord) venueView.showVenues(foundVenues)
            })
        }
    }
}