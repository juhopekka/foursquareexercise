package com.planthegame.foursquareexercise.util

import com.planthegame.foursquareexercise.data.Venue

/**
 * Type aliases of the app
 */

typealias ResultCallback = (Array<Venue>) -> Unit
