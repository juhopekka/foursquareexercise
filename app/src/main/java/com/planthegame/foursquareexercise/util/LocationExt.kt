package com.planthegame.foursquareexercise.util

import android.location.Location
import java.util.*

/**
 * Android [Location] extension functions
 */

fun Location.toRequestParameterFormat() = String.format(Locale.US, "%.5f,%.5f", latitude, longitude)