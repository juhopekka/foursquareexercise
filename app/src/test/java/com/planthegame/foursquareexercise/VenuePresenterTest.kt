package com.planthegame.foursquareexercise

import android.location.Location
import com.planthegame.foursquareexercise.data.source.GpsLocationDataSource
import com.planthegame.foursquareexercise.data.source.FoursquareVenueDataSource
import com.planthegame.foursquareexercise.data.Venue
import junit.framework.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mockito.*

import org.mockito.MockitoAnnotations

/**
 * Test for [VenuePresenter]
 */
class VenuePresenterTest {

    private lateinit var presenter: VenuePresenter

    private lateinit var view: VenueContract.View
    private lateinit var venueSource: FoursquareVenueDataSource
    private lateinit var locationSource: GpsLocationDataSource

    @Captor
    private  lateinit var venueArrayCaptor: ArgumentCaptor<Array<Venue>>

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        view = mock(VenueContract.View::class.java)
        venueSource = mock(FoursquareVenueDataSource::class.java)
        locationSource = mock(GpsLocationDataSource::class.java)
    }

    @Test
    fun initialization_permissionNotGranted() {
        doReturn(false).`when`(view).isLocationPermissionGranted()
        doReturn(true).`when`(locationSource).activate()

        presenter = VenuePresenter(view, venueSource, locationSource)

        verify(view).isLocationPermissionGranted()
        verify(view).askForPermission()
        verify(locationSource, never()).activate()
    }

    @Test
    fun initialization_permissionAlreadyGranted() {
        doReturn(true).`when`(view).isLocationPermissionGranted()

        presenter = VenuePresenter(view, venueSource, locationSource)

        verify(view).isLocationPermissionGranted()
        verify(view, never()).askForPermission()
        verify(locationSource).activate()
    }

    @Test
    fun onPermissionResult_permissionDeniedByUser() {
        presenter = VenuePresenter(view, venueSource, locationSource)
        presenter.onPermissionResult(false)

        verify(locationSource, never()).activate()
        verify(view).shutdownWithLackOfPermissionNote()
    }

    @Test
    fun onPermissionResult_activatingLocationSourceFails() {
        doReturn(false).`when`(locationSource).activate()
        presenter = VenuePresenter(view, venueSource, locationSource)
        presenter.onPermissionResult(false)

        verify(locationSource, never()).activate()
        verify(view).shutdownWithLackOfPermissionNote()
    }

    @Test
    fun onPermissionResult_permissionGrantedByUser() {
        doReturn(true).`when`(locationSource).activate()
        presenter = VenuePresenter(view, venueSource, locationSource)
        presenter.onPermissionResult(true)

        verify(locationSource).activate()
        verify(view, never()).shutdownWithLackOfPermissionNote()
    }

    @Test
    fun searchWordSet_empty() {
        doReturn(true).`when`(view).isLocationPermissionGranted()
        doReturn(mock(Location::class.java)).`when`(locationSource).currentLocation

        presenter = VenuePresenter(view, venueSource, locationSource)
        presenter.searchWordSet("")
        verify(venueSource, never()).getVenuesFor(any(), any(), any())
        verify(view).showVenues(capture(venueArrayCaptor))
        Assert.assertEquals(0, venueArrayCaptor.value.size)
    }

    @Test
    fun searchWordSet_arbitrary() {
        doReturn(true).`when`(view).isLocationPermissionGranted()
        doReturn(mock(Location::class.java)).`when`(locationSource).currentLocation
        val venues = arrayOf(Venue("a", "o", 5),Venue("b", "r", 5),Venue("a", "p", 5))
        doAnswer({ invocation ->
            val callback = invocation.getArgument(2) as (Array<Venue>) -> Unit
            callback.invoke(venues)
        }).`when`(venueSource).getVenuesFor(eq("Abc"), any(), any())

        presenter = VenuePresenter(view, venueSource, locationSource)
        presenter.searchWordSet("Abc")

        verify(venueSource).getVenuesFor(eq("Abc"), any(), any())
        verify(view).showVenues(capture(venueArrayCaptor))
        Assert.assertEquals(venues, venueArrayCaptor.value)
    }

}